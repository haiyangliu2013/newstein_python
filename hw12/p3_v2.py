# You are climbing a stair case. It takes n steps to reach to the top.
# Each time you can either climb 1 or 2 steps. In how many distinct ways can you climb to the top?


class Solution:

    def climbStairs(self, n):
        if n <= 0:
            return 0
        if n == 1:
            return 1
        if n==2:
            return 2
        ret = self.climbStairs(n - 1) + self.climbStairs(n - 2)

        return ret

s = Solution()
print(s.climbStairs(5))

