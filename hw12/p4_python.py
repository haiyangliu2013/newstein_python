# Question 4 in python
# Given a list of n-1, in the range of 1 to n, with no duplicates.
# Write an efficient code to find the missing integer.


def findMissing(l1):
    for i in range(1, max(l1)):
        if i not in l1:
            return i


l1 = [1, 2, 4, 6, 3, 7, 8]
print(findMissing(l1))
