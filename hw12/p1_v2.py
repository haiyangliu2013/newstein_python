# another approach trying to solve question 1
# Given a set of candidate numbers and a target number, find all unique
# combinations in candidates where the candidate numbers sums to target
# The same repeated number may be chosen from candidates unlimited number of times
# Note: 1. All numbers will be positive integers
#       2. The solution set must not contain duplicate combinations


# many duplicates in result
def sigma_set(candidates, target):
    """
    Input: candidates -- a list of unique positive number
           target     -- a desired positive value
    Output: result    -- a list of lists where values in each list sum up to target
    """
    helper = [[candidates[x]] for x in range(len(candidates))]
    result = []

    # computationally expensive
    status = True
    while status:
        length = len(helper)
        status = False

        for i in range(0, length):
            ilist = helper[i]
            ival = sum(ilist)
            if ival == target and ilist not in result:
                result.append(ilist)
            if ival > target:
                continue

            for j in range(i, length):
                jlist = helper[j]
                jval = sum(jlist)
                combined_list = ilist + jlist
                combined_val = ival + jval

                if combined_val < target:
                    if combined_list not in helper:
                        helper.append(combined_list)
                        status = True
                elif combined_val == target:
                    if combined_list not in result:
                        result.append(combined_list)
    return result


if __name__ == "__main__":
    candidates = [1, 2, 3, 4]
    target = 7
    print(sigma_set(candidates, target), "\n")

    candidates = [2, 3, 6, 7]
    target = 7
    print(sigma_set(candidates, target), "\n")

    candidates = [2, 3, 5]
    target = 8
    print(sigma_set(candidates, target), "\n")

    candidates = [2, 3, 5]
    target = 1
    print(sigma_set(candidates, target), "\n")

    candidates = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    target = 10
    print(sigma_set(candidates, target))
