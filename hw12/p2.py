# question 2
# Given a linked list, swap every two adjacent nodes and return its head.
# Note: 1. Your algorithm should use only constant extra space
#       2. You may not modify the values in the list's nodes, only nodes itself maybe changed


class ListNode:
    def __init__(self, value):
        self.value = value
        self.next = None

    def printlist(self):
        while self.next:
            print("{0}->".format(self.value))
            self = self.next
        if self.next is None:
            print("{0}\n".format(self.value))


# Start:  1 --> 2 --> 3 --> 4 --> 5 --> 6
# 1st:    2 --> 1 --> 3 --> 4 --> 5 --> 6 --> 7
# 2nd:    2 --> 1 --> 4 --> 3 --> 5 --> 6 --> 7
# End:    2 --> 1 --> 4 --> 3 --> 6 --> 5 --> 7

def swaplist(head):
    """
    Input: head -- head of a given singly linked list
    Output: head -- head of the swapped list
    """
    if head.next is None:    # if the singly linked list only has one term
        return head
    elif head.next and not head.next.next:
        head, head.next, head.next.next = head.next, head, None
        return head
    elif head.next.next:
        head, head.next, head.next.next = head.next, head, head.next.next

    var = head.next
    while var.next:    # if there are three more terms
        if var.next and not var.next.next:    # if there's one more term
            var, var.next = var, var.next
            break
        elif var.next.next and not var.next.next.next:    # if there are two more terms
            var, var.next, var.next.next, var.next.next.next = \
                var, var.next.next, var.next, None
            break
        elif var.next.next.next:
            var, var.next, var.next.next, var.next.next.next = \
                var, var.next.next, var.next, var.next.next.next
        var = var.next.next

    return head


if __name__ == "__main__":
    n1 = ListNode(1)
    n2 = ListNode(2)
    n3 = ListNode(3)
    n4 = ListNode(4)
    n1.next = n2
    n2.next = n3
    n3.next = n4
    swaplist(n1).printlist()

    n1 = ListNode(1)
    swaplist(n1).printlist()

    n1 = ListNode(1)
    n2 = ListNode(2)
    n3 = ListNode(3)
    n4 = ListNode(4)
    n5 = ListNode(5)
    n6 = ListNode(6)
    n7 = ListNode(7)
    n1.next = n2
    n2.next = n3
    n3.next = n4
    n4.next = n5
    n5.next = n6
    n6.next = n7
    swaplist(n1).printlist()
