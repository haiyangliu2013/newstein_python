# Question 5 in python
# Write a function rotate(arr[], d, n) that rotates arr[]
# of size n by d elements.


def rotate(arr, d):
    arr = arr[d:] + arr[:d]
    return arr


if __name__ == "__main__":
    arr = [1, 2, 3, 4, 5, 6, 7]
    d = 2
    print(rotate(arr, d))

    arr = [1, 4, 2, 8, 5, 7]
    d = 4
    print(rotate(arr, d))
    d = 6
    print(rotate(arr, d))
