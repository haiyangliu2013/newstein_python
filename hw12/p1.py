# question 1
# Given a set of candidates (without duplicates) and a target number (target)
# find all unique combinations in candidates where the candidate numbers sums to target
# the same repeated number may be chosen from candidates unlimited number of times
# Note: all numbers (including target) will be positive integers
# The solution set must not contain duplicate combinations


def sigma_set(candidates, target):
    """
    Input: candidates -- a list of unique positive number
           target     -- a desired positive value
    Output: result    -- a list of lists where values in each list sum up to target
    """
    result = []
    candidates.sort()
    helper = candidates[:]
    # expand the candidates list, e.g. if candidates = [2, 3] and target = 4, expand to [2, 3, 4]
    for i in range(0, len(candidates)):
        if target >= candidates[i]*2:
            max_expansion = target // candidates[i]

            for j in range(2, max_expansion+1):
                if candidates[i]*j not in helper:
                    helper.append(candidates[i]*j)

    helper.sort()

    def checking(element, candidates):
        """
        Mechanism: you know that the element is not in the original list
                   else, return all the possible combinations
        """
        tmp = []
        for k in range(len(candidates)):
            if element % candidates[k] == 0 and element >= candidates[k]*2:
                tmp.extend([[candidates[k]] * (int(element / candidates[k]))])
        return tmp

    for i in range(0, len(helper)):
        if helper[i] == target:
            if helper[i] in candidates:
                result.append([helper[i]])
            else:
                result += checking(helper[i], candidates)

        if i == len(helper)-1:
            break

        for j in range(i+1, len(helper)):    # find all unique pairs
            if helper[i] + helper[j] == target:
                if helper[i] in candidates and helper[j] in candidates:
                    result.append([helper[i], helper[j]])
                elif helper[i] in candidates and helper[j] not in candidates:
                    jlist = checking(helper[j], candidates)
                    for n in range(len(jlist)):
                        result.append([helper[i]] + jlist[n])
                elif helper[i] not in candidates and helper[j] in candidates:
                    ilist = checking(helper[i], candidates)
                    for m in range(len(ilist)):
                        result.append(ilist[m] + [helper[j]])
                else:
                    ilist = checking(helper[i], candidates)
                    jlist = checking(helper[j], candidates)
                    for m in range(len(ilist)):
                        for n in range(len(jlist)):
                            result.append(ilist[m] + jlist[n])

    i = 0
    while i < len(result):    # remove duplicates
        if result.count(result[i]) != 1:
            del result[i]
        i += 1

    return result


if __name__ == "__main__":
    candidates = [2, 3, 6, 7]
    target = 7
    print(sigma_set(candidates, target))

    candidates = [2, 3, 5]
    target = 8
    print(sigma_set(candidates, target))

    candidates = [2, 3, 5, 7, 11, 13, 17, 19]
    target = 12
    print(sigma_set(candidates, target))

    candidates = [2, 3, 5, 7, 11, 13, 17, 19]
    target = 18
    print(sigma_set(candidates, target))

    # result doesn't include [1, 2, 3]
    candidates = [1, 2, 3]
    target = 6
    print(sigma_set(candidates, target))
