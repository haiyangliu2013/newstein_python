# Given a set of candidate numbers (candidates) (without duplicates) and a target number (target),
# find all unique combinations in candidates where the candidate numbers sums to target.
# The same repeated number may be chosen from candidates unlimited number of times.

class Solution:
    def combinationSum(self,candidates,target):
        # create a result list
        self.res=[]
        # invoke helper function, passing source, buffer list for temp answer, target number, index
        self.helper(sorted(candidates), [], target, 0)
        # return the result
        return self.res

    def helper(self, cand, comb, tar, i):
        # flag to check if sum > target
        exceed = False
        # loop over the list
        for j in range(i, len(cand)):
            # if exceed
            if exceed is True:
                break
            # empty list append cand[j]
            comb.append(cand[j])

            if tar-cand[j]>0:
                self.helper(cand, comb, tar-cand[j], j)
            elif tar-cand[j] == 0:
                temp = list(comb)
                self.res.append(temp)
                exceed = True
            else:
                exceed = True
            # clear buffer , backtracking
            comb.pop()
        return