# Given a linked list, swap every two adjacent nodes and return its head.
# Example:
# Given 1->2->3->4, you should return the list as 2->1->4->3.


# ListNode dummy=new ListNode(0);
#         dummy.next=head;
#         ListNode cur=dummy;
#         while(cur.next!=null && cur.next.next!=null){
#             ListNode first= cur.next;
#             ListNode second = cur.next.next;
#             first.next=second.next;
#             cur.next=second;
#             cur.next.next=first;
#
#
#             //second.next=first;
#             cur=cur.next.next;
#
#         }
#         return dummy.next;

class ListNode:
    def __init__(self, val):
        self.val = val
        self.next = None


# Start:  0->   1 --> 2 --> 3 --> 4 --> 5 --> 6
def swap_pairs(head):
    dummy = ListNode(0)
    dummy.next = head
    # current reference
    cur = dummy
    while(cur.next != None and cur.next.next !=None):
        first = cur.next
        second = cur.next.next
        first.next = second.next
        cur.next = second
        cur.next.next = first
        # move the current to next pair
        cur = cur.next.next
    return dummy.next

l1 = ListNode(1)
l2 = ListNode(2)
l3 = ListNode(3)
l4 = ListNode(4)
l5 = ListNode(5)
l6 = ListNode(6)

l1.next = l2
l2.next = l3
l3.next = l4
l4.next = l5
l5.next = l6
l6.next = None

head = swap_pairs(l1)
while(head):
    print(head.val)
    head = head.next
