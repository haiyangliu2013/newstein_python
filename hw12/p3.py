# question 3
# You are climbing a stair case. It takes n steps to reach to the top.
# Each time you can either climb 1 or 2 steps. In how many distinct ways can you climb to the top?
# Given n will be a positive number


def distinct_ways(n):
    """
    Input n: number of stairs
    Output result: number of distinct ways to climb to the top
    """
    helper = [0 for x in range(n)]    # create a dp 1D array
    helper[0] = 1
    helper[1] = 1

    for i in range(0, n):    # for ith index in array, add its value to i+1 and i+2 indices
        if i < n-1:
            helper[i+1] += helper[i]    # climb 1 step
        if i < n-2:
            helper[i+2] += helper[i]    # climb 2 steps

    result = helper[-1]
    return result


if __name__ == "__main__":
    n = 2
    print(distinct_ways(n))

    n = 3
    print(distinct_ways(n))

    n = 5
    print(distinct_ways(n))

    n = 10
    print(distinct_ways(n))

    n = 20
    print(distinct_ways(n))

    n = 50
    print(distinct_ways(n))

    n = 100
    print(distinct_ways(n))
