# Given an array of integers nums sorted in ascending order, find the starting and endinng
# position of a given target value. Time complexity: O(log n)
# If the target is not found in the array, return [-1. -1]


def find(list1, target):
    """
    Input: A list sorted in ascending order and a target value
    Output: [start position, end position]
    """

    result = [-1, -1]
    head = 0
    tail = len(list1)-1

    if list1 is None:    # if the list is empty
        return result
    if target < list1[0] or target > list1[-1]:    # if target is smaller than the smallest value or larger than the largest value
        return result

    while head <= tail and (result[0] == -1 or result[1] == -1):
        mid = (head + tail) // 2

        if list1[head] == target and result[0] == -1:
            if head == 0:    # if the item is the first element
                result[0] = head
            elif list1[head-1] != target:    # if the term before head is not target, store head
                result[0] = head
            else:    # if the term before head is target, go back one step
                head = head-1

        elif list1[tail] == target and result[1] == -1:
            if tail == len(list1)-1:    # if the item is the last element
                result[1] = tail
            elif list1[tail+1] != target:    # if the term after tail is not target, store tail
                result[1] = tail
            else:    # if the term after head is not target, go back one step
                tail = tail+1

        elif list1[head] == target and list1[head+1] != target and result[1] == -1:    # if the term after head is not target, store head
            result[1] = tail
        elif list1[tail] == target and list1[tail-1] != target and result[0] == -1:    # if the term before tail is not target, store tail
            result[0] = tail
        elif list1[head] <= target <= list1[mid] and result[1] == -1:
            tail = mid
        elif list1[mid] <= target <= list1[tail] and result[0] == -1:
            head = mid

        elif head == tail-1:
            if list1[head] < target < list1[tail]:
                return result

    return result


if __name__ == '__main__':
    nums = [5, 7, 7, 8, 8, 10]
    print(find(nums, 7))
    print(find(nums, 11))
    print(find(nums, 9))    # having bug

    nums = [1, 2, 2, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 5, 6, 6, 6, 6, 6, 6]
    print(find(nums, 6))

    nums = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
    print(find(nums, 1))
