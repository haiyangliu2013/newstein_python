# question 5
# Given a non-empty string s and a dictionary wordDict containing a list of non-empty words,
# determine if s can be segmented into a space-separated sequence of one or more
# dictionary words


# a straight-forward way

def seperate(string, wordDict):

    length = len(string)
    ind = 0    # starting from the beginning of the string

    while ind <= length:
        if ind == length:
            return True

        for exten in range(ind, length):
            if string[ind:exten+1] in wordDict:
                ind = exten+1
                print(ind)
                break
        else:
            return False


if __name__ == '__main__':
    s = 'leetcode'
    wordDict = ['leet', 'code']
    print(seperate(s, wordDict))

    s = 'applepenapple'
    wordDict = ['apple', 'pen']
    print(seperate(s, wordDict))

    s = 'sandctdog'
    wordDict = ['cats', 'dog', 'sand', 'and', 'cat']
    print(seperate(s, wordDict))


