# Given an array of integers nums sorted in ascending order, find the starting and ending position of a given target value.
# Your algorithm's runtime complexity must be in the order of O(log n).
# If the target is not found in the array, return [-1, -1].

def searchRange(nums, target):
    def search(n):
        lo, hi = 0, len(nums)
        while lo < hi:
            mid = (lo + hi) // 2
            if nums[mid] >= n:
                hi = mid
            else:
                lo = mid + 1
        print("lo",lo)
        return lo
    lo = search(target)
    #print("lo", lo)
    return [lo, search(target+1)-1] if target in nums[lo:lo+1] else [-1, -1]

l = [5,7,7,8,8,8, 8, 8, 15,20,30]
print(searchRange(l, 8))