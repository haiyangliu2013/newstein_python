# Given a non - empty string s and a dictionary wordDict containing
# a list of non - empty words, determine if scan be segmented into a space - separated sequence of one or more dictionary words.

def word_break(s, words):
    d = [False] * len(s)
    for i in range(len(s)):
        for w in words:
            if w == s[i+1-len(w):i+1]:
                # previous word match or it is the first word
                if (d[i-len(w)] or i-len(w)==-1):
                    d[i] = True
    return d[-1]

if __name__ == '__main__':
    s = 'leetcode'
    wordDict = ['leet', 'code']
    print(word_break(s, wordDict))

    s = 'applepenapple'
    wordDict = ['apple', 'pen']
    print(word_break(s, wordDict))

    s = 'sandctdog'
    wordDict = ['cats', 'dog', 'sand', 'and', 'cat']
    print(word_break(s, wordDict))