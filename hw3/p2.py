# question 2

# write a python program that parses the given file (100.txt)
# and returns (and prints) a dictionary where keys are the words in the text
# and the values are the word counts
# A word is defined by the space characters around it
# you should ignore the empty lines

# use the dictionary you created in the previous question
# find the 10 most common words
# and write them into "top10_words.txt" file, with the words and their corresponding counts
# separated with commas


# Program Failed...
dict_words = {}    # set up a dictionary
file = open('100-0.txt')


file_list = file.readlines()    # have a list of strings
num_row = len(file_list)
index_row = 0
value = 0

while index_row < num_row:    # row index starting from 1

    file_single_line = file_list[index_row]    # get the line corresponding to the row
    num_column = len(file_single_line)

    index_column = 0
    temp_list = []    # becomes empty at the beginning of each column swipe

    if len(file_single_line) != 0:    # if the column is not empty
        while index_column < num_column - 1:

            char = file_list[index_row][index_column]    # look at a single character

            if char != ' ' and file_list[index_row][index_column + 1] != ' ':    # inside a word
                temp_list.append(char)

            elif char != ' ' and file_list[index_row][index_column + 1] == ' ':    # the last letter
                temp_list.append(char)
                key = ''.join(temp_list)    # make a list of letters into a word

                dict_words[value] = key

                temp_list = []    # clear the list

                value += 1

            index_column += 1
        index_row += 1


print(dict_words)
# when I check back, the last word of each line is always missed. Couldn't figure out.
