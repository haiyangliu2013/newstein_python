# you are given an n x n 2D matrix. Rotate the matrix by 90 degrees (clockwise) and print
# note: you have to rotate the matrix in-place, which means you have to modify
# the input 2D matrix directly.

import numpy as np


def rotate_clockwise(matrix):
    "Rotate an nxn 2D matrix by 90 degrees clockwise"

    if len(matrix) == 1:    # if matrix is a 1x1 matrix
        print(np.array(matrix))
        return

    matrix = np.array(matrix)    # turn list of lists into matrix with arrays
    matrix = matrix.transpose()    # take the transpose
    # print('initial matrix:\n', matrix)

    (num_row, num_column) = matrix.shape    # record dimensions
    # print(num_row)    # Check
    # print(type(matrix[1][0]))

    # want to reverse elements on each row
    for index_row in range(num_row):    # swipe by row
        if num_column % 2 == 0:    # even number for number of column
            num_pair = num_column / 2

        if num_column % 2 == 1:    # odd number for number of column
            num_pair = (num_column - 1) / 2

        for index_column in range(int(num_pair)):   # swipe through the first half of the column, link to their pair
            element_forward = matrix[index_row][index_column]
            element_backward = matrix[index_row][(num_column-1) - index_column]

            (matrix[index_row][index_column], matrix[index_row][(num_column-1) - index_column]) \
                = (element_backward, element_forward)    # a, b = b, a

    print(matrix)




a = [[1, 2, 3],
     [4, 5, 6],
     [7, 8, 9]]

b = [2]

c = [[5, 1, 9, 11],
     [2, 4, 8, 10],
     [13, 3, 6, 7],
     [15, 14, 12, 16]]

d = [[1, 0, 0, 0, 0], [0, 1, 0, 0, 0], [0, 0 ,1, 0, 0], [0, 0, 0, 1, 0], [0, 0, 0, 0, 1]]


rotate_clockwise(a)
