import operator

f = open("100-0.txt", 'r') #open("test.txt", 'r')  #open("100-0.txt", 'r')
w = open("top10words.txt",'w')
myDict = {}
for eachLine in f:
    l2 = " ".join(eachLine.split())
    l3 = l2.strip()
    # l3.decode('utf8')
    array = l3.split(" ")
    for item in array:
        if item =='':
            continue
        if item in myDict:
            myDict[item] += 1
        else:
            myDict[item] = 1

sorted_d = sorted(myDict.items(), key=operator.itemgetter(1), reverse=True)


for i in range (0,10):
    w.write(str(sorted_d[i]) )
    w.write(", ")


