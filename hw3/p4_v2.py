class Solution:
    def rotate(self, matrix):
        """
        :type matrix: List[List[int]]
        :rtype: void Do not return anything, modify matrix in-place instead.
        """
        col= row = len(matrix)
        # matrix = list(reversed(matrix))
        for i in range(row // 2):
            matrix[i], matrix[row-1-i] = matrix[row-1-i], matrix[i]
        # print (matrix)

        for i in range(0,row-1):
            for j in range(i+1,col):
                if i != j:
                    matrix[i][j], matrix[j][i] = matrix[j][i], matrix[i][j]
        print (matrix)

s = Solution()

input1=[
  [ 5, 1, 9,11],
  [ 2, 4, 8,10],
  [13, 3, 6, 7],
  [15,14,12,16]
]

input2=[
  [15,13, 2, 5],
  [14, 3, 4, 1],
  [12, 6, 8, 9],
  [16, 7,10,11]
]
