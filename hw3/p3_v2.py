def pascal_trangle(input):
    result=[]
    if input<=0:
        return result
    for i in range (input):
        row=[]
        for j in range (i+1):
            if j==0 or j==i:
                row.append(1)
            else:
                row.append(result[i-1][j-1]+result[i-1][j])
        result.append(row[:])
    return result

result = pascal_trangle(5)
for n in result:
    print(n,"\n")

