import datetime
import csv

with open('prices_sample.csv') as csvFile:
	dst=open('datetimes.txt','w')
	csvReader = csv.reader(csvFile)
	for row in csvReader:
		date = datetime.datetime.fromtimestamp(int(row[0])).strftime('%Y-%m-%d %H:%M:%S')
		dst.write(date+', ')
		dst.write(row[1])
		dst.write("\n")

	dst.close()
csvFile.close()
