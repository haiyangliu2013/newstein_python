# question 3
# given a non-negative integer numRows, generate the Pascal's triangle up to numRows
# do not use math formula to calculate the Pascal's triangle directory


def Pascal_trig(numRows):
    "Prints a Pascal Triangle with numRows rows"

    trig_list = []
    trig_list.append([1])    # the first and the second row
    trig_list.append([1, 1])

    # print(str(trig_list))    # check the initial state

    for index_row in range(2, numRows):    # starting with the third row
        num_element = index_row + 1    # index_row starts from 2, num_element starts from 3 (the third row)
        list_single_row = []    # create a temporary list for each row

        for index_column in range(num_element):  # swipe along a row

            if index_column == 0:    # first element
                list_single_row.append(1)

            elif index_column == num_element-1:    # last element
                list_single_row.append(1)
                trig_list.append(list_single_row)    # add current row's list into trig_list

            else:    # elements other than the first and the last ones
                element_up_left = trig_list[index_row-1][index_column-1]
                element_up_right = trig_list[index_row-1][index_column]
                element = element_up_left + element_up_right    # result from the sum of upper left and upper right elements

                list_single_row.append(element)

    # print(trig_list)    # check the list

    for index_row in range(numRows):   # print out the triangle row by row
        len_current_row = len(trig_list[index_row])
        len_longest_row = len(trig_list[numRows-1])
        len_indent_diff = (len_longest_row - len_current_row)
        # print(len_indent_diff)

        print(' ' * len_indent_diff, (trig_list[index_row]))    # try to make a triangle





Pascal_trig(6)

# Pascal_trig(0)

# Pascal_trig(2)

# Pascal_trig(100)

# Pascal_trig(10000)
