# question 1

# you are provided with a file (prices_sample.csv) where each line is time
# and the corresponding Bitcoin value
# First, you will need to convert the time to a more desirable format
# the timestamps are expressed in UNIX epoch, which means it denotes the number of seconds
# passed since Jan 1, 1970 (e.g, Jan 1, 1970 means 0). You need to
# a. convert this to a data time format, Python already provides functionality for this conversion
#    Find that out, convert each timestamp in the file to actual data and time
# b. write it back to another file named datatimes.txt


import datetime


file = open('prices_sample.csv', 'r')
new_file = open('datatimes.txt', 'w')

file_list = file.readlines()   # get a list of lists
file_len = len(file_list)
# print(file_len), output = 150000

index = 0
# print(file_list[0][0:9])    # from this, know [1514934546, 14850.99...] is a string

while index < file_len:    # looping through each pair (Unix epoch + Bit coin Value)
    unix_time = int(file_list[index][0:9])
    bit_value = file_list[index][11:]
    std_time = datetime.datetime.fromtimestamp(unix_time)

    new_file.writelines((str(std_time),', ', bit_value))
    index = index + 1


file.close()
new_file.close()


with open('datatimes.txt', 'r') as new_file:   # automatically close the file
    new_file_len = len(new_file.readlines())

    if new_file_len == file_len:
        print('Length Matched')
    else:
        print('Length does not match, Check Again')

