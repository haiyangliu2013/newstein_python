# question 4
# A palindrome is a sequence of characters or numbers that looks the same forward and backwards
# for example, "Madam, I'm Adam" is a palindrome (should ignore punctuation)
# because it is spelled the same reading it from front to back as from back to front.
# the number 12321 is a numerical palindrome

# write a function that takes a string and its length as arguments and recursively
# determines whether the string is a palindrome. Use Recursion


def filter(rstring):
    """
    :param a raw string:
    :return: raw string without punctuations and spaces
    """
    len_str = len(rstring)
    tmp_list = []

    for index in range(len_str):
        if rstring[index].isdigit():    # only keep alphabets and digits (integers)
            tmp_list.append(rstring[index])
        if rstring[index].isalpha():
            tmp_list.append(rstring[index].lower())

    filtered_str = ''.join(tmp_list)
    return filtered_str


def ispalindrome(string):
    """
    :param string:
    :return: True if input is a palindrome, False otherwise
    """
    if len(string) is 1:    # string with odd length
        return True
    elif len(string) is 2:    # string with even length
        return string[0] == string[1]
    else:
        shorter_string = string[1:-1]    # define a shorter string by removing its first and last element
        return (string[0] == string[-1]) and ispalindrome(shorter_string)


if __name__ == '__main__':
    a = '2.. 13, 4   , 23 // /4'
    a = filter(a)
    print(ispalindrome(a))

    b = '1 , 3, 2 ,44 ,2, 3 ;;;; /// 1'
    b = filter(b)
    print(ispalindrome(b))

    c = "Madam, I'm Adam"
    c = filter(c)
    print(c)
    print(ispalindrome(c))

    d = 'hello world'
    d = filter(d)
    print(ispalindrome(d))
