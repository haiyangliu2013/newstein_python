# Given a positive integer n, generate a square matrix filled with elements from 1 to n2 in spiral order.

class Solution:
    def generateMatrix(self, n):
        """
        :type n: int
        :rtype: List[List[int]]
        """
        top, bottom, left, right = 0,n-1,0, n-1
        res = [[0]*n for _ in range(n)]
        count = 1
        i,j = 0,0  # i = row reference     j = col reference
        while count <= n*n:
            if i == top:
                for j in range (j, right+1):
                    res[i][j] = count
                    count+=1
                top+=1
                i+=1
            if j == right:
                for i in range (i, bottom+1):
                    res[i][j] = count
                    count+=1
                right-=1
                j-=1
            if i==bottom:
                for j in range(j,left-1, -1):
                    res[i][j] = count
                    count+=1
                bottom-=1
                i-=1
            if j==left:
                for i in range(i,top-1,-1):
                    res[i][j] = count
                    count+=1
                left+=1
                j+=1
        return res

if __name__ == "__main__":
    s = Solution()
    n = 5
    result = s.generateMatrix(n)
    for l in result:
        print(l,'\n')
