# question 3
# use generator and yield to write a function which reverse a
# string and print




def mechanics(input):
    "If input is a string, yields the elements from right to left."
    if type(input) is not str:
        return

    length = len(input)
    index = length - 1    # starting from the last term

    while index >= 0:    # swipes backward
        yield input[index]
        index -= 1


def reverse_str(input):
    "If input is a string, reverse it and print it."
    print(''.join(list(mechanics(input))))



reverse_str('hello world')
reverse_str('abcdefghijklmn')
reverse_str('')
reverse_str(232)
reverse_str('1124234 32423432 34235432 2 234234')

