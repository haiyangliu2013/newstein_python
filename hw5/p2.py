# given an array nums of n integers and an integer target
# find three integers in nums such that the sum is closest to target
# return the sum of the three integers
# you may assume that each input would have exactly one solution


def sum_3(input, target):
    """
    :param input: a list containing numbers
    :param target: a desired sum
    :return: the sum closest to target and the corresponding list
    """
    input.sort()
    result = [input[0], input[1], input[2]]    # initialize
    length = len(input)
    # print(input)

    for i in range(length-2):   # three loops, i j k
        # ignore duplicated element
        if i > 0 and input[i] == input[i-1]:
            continue

        for j in range(i+1, length-1):
            for k in range(j+1, length):

                if abs(input[i] + input[j] + input[k] - target) < abs(sum(result) - target):    # if distance is smaller
                    result[0], result[1], result[2] = input[i], input[j], input[k]
                    if input[i] + input[j] + input[k] - target == 0:
                        return 'The sum that is closest to the target is {0}. ({1})'.format(sum(result), result)

    return 'The sum that is closest to the target is {0}. ({1})'.format(sum(result), result)


if __name__ == '__main__':
    a = [-1, 0, 1, 2, -1, 4]
    print(sum_3(a, 2))

    b = [1, 7, 4, 5, 4, 1, 3, 4]
    print(sum_3(b, 0))
    print(sum_3(b, 100))
    print(sum_3(b, 14))

    c = [0, 5, 10, 15]
    print(sum_3(c, 18))
    print(sum_3(c, 17))

    d = [i for i in range(100)]
    print(sum_3(d, 1000000))

