# A palindrome is a sequence of characters or numbers that looks the same forwards and backwards.
# For example, "Madam, I'm Adam" is a palindrome (should Ignore punctuation ) because it is spelled the same reading it from front to back as from back to front.
# The number 12321 is a numerical palindrome. Write a function that takes a string and its length as arguments and recursively determines whether the string is a palindrome.


def isPalindrome(input):
    if input is None or len(input)==0:
        return True
    punctuations = '''!()-[]{};:'"\,<>./?@#$%^&*_~'''
    no_punct = ""
    for char in input:
        if char not in punctuations:
            no_punct = no_punct + char
    l = 0
    r = len(no_punct)-1

    return isPalindromeHelper(no_punct, l, r)

def isPalindromeHelper(input, l, r):
    if(l==r):
        return True

    elif input[l] != input[r]:
        return False

    else:
        return isPalindromeHelper(input, (l+1) , (r-1))

if __name__ == "__main__":
    input="1232"
    print(isPalindrome(input))
