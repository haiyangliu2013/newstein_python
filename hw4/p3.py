# question 3
# matrix spiral II: given a positive integer n, generate a square matrix
# filled with elements from 1 to n^2 in spiral order


def square_matrix(n):
    """
    :Type matrix: List[List[int]]
    :return elements from 1 to n^2 in spiral order:
    """
    num_row = num_column = int(n)
    matrix = []

    for index_row in range(num_row):
        tmp_row = []    # each column in a matrix

        for index_column in range(num_column):
            element = (index_column+1) + index_row * num_column    # arrange in spiral order
            tmp_row.append(element)

        matrix.append(tmp_row)

    # print matrix
    for index_row in range(num_row):
        print(matrix[index_row])


square_matrix(6)

square_matrix(10)

square_matrix(0)

