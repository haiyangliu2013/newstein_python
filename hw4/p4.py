# question 4
# given an array nums of n integers, are there elements a,b,c,d in nums such that
# a+b+c+d = 0? Find all unique pairs in the array which gives the sum of zero


def sum_4(input):
    result = []
    input.sort()
    leng = len(input)

    for layer1 in range(0, leng-3):    # the outer layer, swipes from left to right
        if (layer1-1) >= 0 and input[layer1] == input[layer1 - 1]:
            continue

        layer2_list = []
        for layer2 in range(layer1+1, leng-2):    # layer2, swipes from left to right
            if input[layer2] in layer2_list:    # if the value for layer2 is used before, skip
                continue

            layer2_list.append(layer2)
            layer3 = layer2 + 1    # layer 3 swipes from left to right
            layer4 = leng - 1    # layer 4 swipes from right to left

            while layer3 < layer4:
                tmp_list = []

                if input[layer1] + input[layer2] + input[layer3] + input[layer4] == 0:
                    tmp_list.extend((input[layer1], input[layer2], input[layer3], input[layer4]))
                    result.append(tmp_list[:])
                    layer3+=1
                    layer4-=1

                    while layer3 < layer4 and input[layer3] == input[layer3 - 1]:
                        layer3+=1

                    while layer3 < layer4 and input[layer4] == input[layer4 + 1]:
                        layer4-=1

                elif (input[layer1] + input[layer2] + input[layer3] + input[layer4]) > 0:
                    layer4-=1
                else:
                    layer3+=1

    for i in range(len(result)):
        print(result[i])



if __name__ == "__main__":
    a = [i for i in range(-5, 6)]
    sum_4(a)

    # b = [-1, 0, 1, 2, -1, -4]
    # sum_4(b)

    # the result have duplicated outputs
    # c = [-1, -1, -1, -1, -1, -1, 0, 0, 0, 0 ,0 , 0, 1, 1, 1, 1, 1]
    # sum_4(c)
