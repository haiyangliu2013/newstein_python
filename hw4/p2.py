# question 2
# say you have an array for which the ith element is the price of a given stock
# on day i. if you were only permitted to complete at most one transaction
# (i.e. buy one and sell one share of the stock), design an algorithm
# to find the maximum profit. Note that you cannot sell a stock before you buy one


# find all possible profits, then find the maximum.
def max_profit(dict1):
    list_sv = list(dict1.values())    # sv for stockvalue
    length = len(list_sv)
    list_diff = []    # storing all possible profit, or difference between the selling price and the buying price

    for layer1 in range(0, length-1):    # buy
        for layer2 in range(layer1+1, length):    # sell

            if list_sv[layer1] < list_sv[layer2]:
                profit = list_sv[layer2] - list_sv[layer1]
                list_diff.append(profit)

    if len(list_diff) == 0:
        return 'No profit'
    else:
        maximum = max(list_diff)    # find max profit
        return maximum


if __name__ == '__main__':
    input = {'Monday':5, 'Tuesday':4, 'Wednesday':3, 'Thursday':2, 'Friday':1}
    print(max_profit(input))

    input = {'Monday':1, 'Tuesday':10, 'Wednesday':6, 'Thursday':9, 'Friday':2}
    print(max_profit(input))

    input = {'Monday':1, 'Tuesday':3, 'Wednesday':100, 'Thursday':5, 'Friday':7}
    print(max_profit(input))

    input = {'Monday':-10, 'Tuesday':4, 'Wednesday':9, 'Thursday':-12, 'Friday':3}
    print(max_profit(input))







# # bad program, if I have time, I will write a new one
# def max_profit(dict1):
#     # sv for stockvalue
#     list_sv = list(dict1.values())
#     while int(len(list_sv)) >= 2:
#         tmp_max = max(list_sv)
#         tmp_min = min(list_sv)
#
#         # find the number of occurrence
#         num_occ_max = int(list_sv.count(tmp_max))
#         num_occ_min = int(list_sv.count(tmp_min))
#
#         # if the price of a given stock is constant
#         if num_occ_max == num_occ_min == len(list_sv):
#             profit = 0
#             return profit
#
#         # find the indexes and compare them
#         # if the first index of min is smaller than the first index of max, chronologically logical
#         if list_sv.index(tmp_min) < list_sv.index(tmp_max):
#             profit = tmp_max - tmp_min
#             return profit
#
#         # if not, remove the first occurrence of max
#         elif list_sv.index(tmp_min) > list_sv.index(tmp_max):
#             del list_sv[list_sv.index(tmp_max)]
