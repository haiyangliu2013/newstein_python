#  Say you have an array for which the ith element is the price of a given stock on day i.
# If you were only permitted to complete at most one transaction (i.e., buy one and sell one share of the stock), design an algorithm to find the maximum profit.
# Note that you cannot sell a stock before you buy one.
# Input: {‘Monday’:7,’Tuesday’:1,’Wednesday’:5,’Thursday’:3,’Friday’:6}
# 	Output: 5
# 	Explanation: Buy on day 2 (price = 1) and sell on day 5 (price = 6),
# profit = 6-1 = 5.
#   Not 7-1 = 6, as selling price needs to be larger than buying price.

def sell_stock(prices):
    max_profit, min_price = 0, float('inf')
    for day, price in prices.items():
        min_price = min(min_price, price)
        profit = price - min_price
        max_profit = max(max_profit, profit)
    return max_profit

if __name__ == "__main__":
    dict = {'Monday':7,'Tuesday':1,'Wednesday':5,'Thursday':3,'Friday':6}
    print(sell_stock(dict))