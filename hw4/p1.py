# question 1

# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
# a. map(), use lambda and map() to return a list of sum of two lists are added


def adding_lists(list1, list2):
    """
    :list1 should contain numbers only
    :list2 should contain numbers only
    :else: return []
    """
    try:
        return list(map(lambda num1, num2: num1 + num2, list1, list2))
    except:
        return []


if __name__ == '__main__':
    list1= [1, 2, 3]
    list2= [2.5, 3]
    print(adding_lists(list1, list2))

    a = []
    b = [1, 2, 3]
    print(adding_lists(a, b))

    a = [1, 2, 3, 4, 5, 6]
    b = [-1, -2, -3, -4, -5, -6]
    print(adding_lists(a, b))


# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
# b. filter(), use lambda and filter() to return only prime number from a list


def find_prime(n):
    """
    :return True if a number is prime
    :return False if a number is not prime
    """
    num = 2    # starting from 2
    prime = True

    while num <= (float(n ** 1/2)) and prime:    # symmetry
        if n % num == 0:    # if a reminder (other than 1 or n) is 0, the number is not prime
            prime = False
            return False
        num += 1
    return True


def find_prime_list(list1):
    """
    :filter -- only prime numbers can remain in the list
    :return [] if there is no prime number
    """
    return list(filter(find_prime, list1))


print(find_prime(97))
print(find_prime(7))

a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
print(find_prime_list(a))

b = [i for i in range(1, 1000) if i % 2 == 0]
print(find_prime_list(b))

c = [i for i in range(1, 10000)]
print(find_prime_list(c))
print(len(find_prime_list(c)))    # internet says there are 1230 prime numbers in [1, 10000]


# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
# c. reduce(), use lambda and reduce() to return Least common multiple of a list

def least_common_multiple(a, b):
    "Returns the least common multiple of a, b"
    multiple = a * b
    for num in range(max(a, b), round(multiple/2) +1):
        if num % a == 0 and num % b == 0:
            return num
    return multiple    # if there's no smaller common multiple


# need to call functools to use reduce()
import functools


def lcm_list(list1):
    "Returns the least common multiple of numbers in a list"
    return functools.reduce(least_common_multiple, list1)


print(least_common_multiple(10, 20))
print(least_common_multiple(20, 37))

a = [1, 12, 34, 42, 42, 23, 17]
print(lcm_list(a))

b = [i for i in range(1, 20) if i % 2 == 0]
print(lcm_list(b))
