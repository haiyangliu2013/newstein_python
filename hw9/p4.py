def maxvalue(val, wt, W, n):

    if n is 0 or W is 0:    # if last element or no more weight
        return 0
    elif wt[n-1] <= W:    # if weight <= allowance, consider two cases: add the item or not
        return max(maxvalue(val, wt, W-wt[n-1], n-1) + val[n-1], maxvalue(val, wt, W, n-1))
    else:    # if weight > allowance, can't add more weight, thus value stays the same
        return maxvalue(val, wt, W, n-1)


if __name__ == '__main__':
    val = [1, 2, 3, 4, 5, 8]
    wt = [10, 20, 30, 40, 50, 70]
    W = 100
    print(maxvalue(val, wt, W, 6))