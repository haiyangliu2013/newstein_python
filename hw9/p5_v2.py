# build-in function


# bubble sort
class largestNumber:

    def compare(self, n1, n2):
        return str(n1) + str(n2) > str(n2) + str(n1)


    # selection sort
    def largestNumber3(self, nums):

        for i in range(len(nums), 0, -1):
            tmp = 0
            # tmp is to store the smallest start number
            # search the smallest start number, put at last
            for j in range(i):
                if not self.compare(nums[j], nums[tmp]):
                    tmp = j
            # switch the last number and the smallest start number
            nums[tmp], nums[i - 1] = nums[i - 1], nums[tmp]
        return str(int("".join(map(str, nums))))


#l = [3,30,34,5,9]



    # merge sort
    def largestNumber5(self, nums):
        nums = self.mergeSort(nums, 0, len(nums) - 1)
        return str(int("".join(map(str, nums))))


    def mergeSort(self, nums, l, r):
        if l > r:
            return
        if l == r:
            return [nums[l]]
        mid = l + (r - l) // 2
        left = self.mergeSort(nums, l, mid)
        right = self.mergeSort(nums, mid + 1, r)
        return self.merge(left, right)


    def merge(self, l1, l2):
        res, i, j = [], 0, 0
        while i < len(l1) and j < len(l2):
            if not self.compare(l1[i], l2[j]):
                res.append(l2[j])
                j += 1
            else:
                res.append(l1[i])
                i += 1
        res.extend(l1[i:] or l2[j:])
        return res


    # quick sort, in-place
    def largestNumber(self, nums):
        self.quickSort(nums, 0, len(nums) - 1)
        return str(int("".join(map(str, nums))))


    def quickSort(self, nums, l, r):
        if l >= r:
            return
        pos = self.partition(nums, l, r)
        self.quickSort(nums, l, pos - 1)
        self.quickSort(nums, pos + 1, r)


    def partition(self, nums, l, r):
        low = l
        while l < r:
            if self.compare(nums[l], nums[r]):
                nums[l], nums[low] = nums[low], nums[l]
                low += 1
            l += 1
        nums[low], nums[r] = nums[r], nums[low]
        return low

obj = largestNumber()
l = [3,30,34,5,9]
res = obj.largestNumber3(l)
print(res)