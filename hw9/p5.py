
# question 5


def arrange(l):
    """
    given a list of non-negative integers,
    arrange them such that they form the largest number.
    """
    list1 = l[:]    # make a copy, save the original list
    maxi = max(list1)
    dup_list = []

    for i in range(len(list1)):
        list1[i] = str(list1[i])

        if len(list1[i]) < len(str(maxi)):    # consider 4 as 444
            dup_list.append(list1[i])
            list1[i] = list1[i] + list1[i][-1] * (len(str(maxi))-len(list1[i]))
            dup_list.append(list1[i])    # record indexes of elements that has been modified

    j = len(str(maxi)) - 1
    while j >= 0:    # sort the list
        list1 = sorted(list1, key=lambda x: x[j], reverse=True)
        j -= 1

    for k in range(int(len(dup_list)/2)):    # turn the modified elements back to their original forms
        ind = list1.index(dup_list[2*k+1])
        list1[ind] = dup_list[2*k]

    result = ''.join(list1)
    return result


if __name__ == '__main__':
    a = [1, 2, 36, 31, 3, 4, 43, 5]
    print(arrange(a))

    b = [4, 53, 435, 341, 235, 532, 839, 602, 340, 2237, 2344]
    print(arrange(b))