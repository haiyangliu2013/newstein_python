# Question 3
# Write a Python program to find out all unique values in a list of dictionary
# switch the key and value


# First way
# This way gets output in the right order, but I couldn't find a way to build dictionaries inside a list.
def findAndSwitch(list1):
    try:
        if list == []:
            return 'Empty input'

        len1 = len(list1)

        # first convert
        for i in range(len1):
            for (a, b) in list1[i].items():
                list1.insert(i+1, [a, b])
                list1.remove(list1[i])
        # return list1

        # if len(list1) == m:
        #     print('Yes')

        for i in range(len1):
            for j in range(2):
                list1.append(list1[0].pop())
            list1.remove(list1[0])
        # return list1

        # collect the duplicates
        len2 = len(list1)
        wl = []    # list for indexes of duplicated values
        for i in range(len2):
            if list1.count(list1[i]) == 1:
                pass
            if list1.count(list1[i]) != 1:
                wl.append(i)
        # return wl

        # manipulate the duplicates
        len3 = len(wl)
        if len3 == 0:
            pass
        if len3 != 0:
            wl.reverse()
            for i in wl:
                list1.remove(list1[i])     # remove from the back so that the indexes don't flow around
                list1.remove(list1[i-1])   # remove the term before it (originally the key)
        return list1


    except:
        return 'Check your input'


list1 = [{'a':'apple'}, {'b':'boy'}, {'c':'apple'},{'d':'dog'}, {'f':'five'}, {'e':'egg'}, {'g':'dog'}]
x = [{'a':'2'}, {'b': '3'}, {'c': '4'}]
y = []
z = [{'':''}, {'':'2'}]

print(findAndSwitch(list1))










# Second way (maybe discuss)
# def findAndSwitch(list):
#     if list == [] :
#         return 'Empty input'
#     list1 = list[:]
#     m = len(list1)
#
#     # first convert
#     for i in range(m):
#         for (a, b) in list1[i].items():
#             list1.insert(i+1, [a, b])
#             list1.remove(list1[i])
#     # return list1
#
#     for i in range(m):
#         for j in range(2):
#             list1.append(list1[0].pop())
#         list1.remove(list1[0])
#     # return list1
#
#     # collect the duplicates
#     n = len(list1)
#     wl = []    # list for indexes of duplicated values
#     for i in range(n):
#         if list1.count(list1[i]) == 1:
#             pass
#         if list1.count(list1[i]) != 1:
#             wl.append(int(i/2))
#     # return wl
#
#     for i in wl:
#         del list[i]
#
#     return list
#
#
# list1 = [{'a':'apple'}, {'b':'boy'}, {'c':'apple'},{'d':'dog'}, {'f':'five'}, {'e':'egg'}, {'g':'dog'}]
# x = [{'a':'2'}, {'b': '3'}, {'c': '4'}]
# y = []
# z = [{'':''}, {'':'2'}]
#
# print(findAndSwitch(list1))
