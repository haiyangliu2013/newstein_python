# Question 3
# Write a Python program to find out all unique values in a list of dictionary
# switch the key and value


# First way
# This way gets output in the right order, but I couldn't find a way to build dictionaries inside a list.
def findAndSwitch(list1):
    try:
        if list1 == []:
            return 'Empty input'


        global dup # check duplicate element , boolean , if found, true , else false

        new_list=[list1[0]]

        i = 1
        while i<len(list1):
            # get the key set of a dictionary
            curr_key = list(list1[i].keys())[0]
            curr_val = list1[i].get(curr_key)
            dup = False
            j = 0
            while j<i:
                prev_key = list(list1[j].keys())[0]
                prev_val = list1[j].get(prev_key) # single value
                if curr_val == prev_val: # duplicate element found
                    dup = True
                    break
                j+=1

            if dup == False:
                new_list.append(list1[i])
            else:
                new_list.remove(list1[j])
            i+=1

        print(new_list)

        for i in range(len(new_list)):
            new_list[i] = {y:x for x,y in new_list[i].items()}
        return new_list

    except:
        return 'Check your input'


list1 = [{'a':'apple'}, {'b':'boy'}, {'c':'apple'},{'d':'dog'}, {'f':'five'}, {'e':'egg'}, {'g':'dog'}]
x = [{'a':'2'}, {'b': '3'}, {'c': '4'}]
y = []
z = [{'':''}, {'':'2'}]

print(findAndSwitch(list1))
