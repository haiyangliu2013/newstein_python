# question 5


def common_prefix(list1):
    """
    Finds the longest common prefix among an array of strings
    If there is no common prefix, return ""
    """
    if list1 is []:
        return

    len1 = len(list1)
    list_length = []

    for i in range(len1):   # put lengths of strings into a new list
        if type(list1[i]) == str:
            list_length.append(len(list1[i]))

    if len(list_length) != 0:
        limit_length_prefix = min(list_length)  # find the minimum length, which is also the maximum length for common prefix

        prefix = ''

        for i in range(limit_length_prefix):    # outer loop -- goes through the index of strings
            letters = []

            for j in range(len1):   # inner loop -- goes through strings
                letters.append(list1[j][i])     # put the corresponding letters into a temporary list
            # print(letters)
            if letters.count(letters[0]) == len1:   # if all letters are the same, like [f, f, f], add to the prefix
                prefix += letters[0]
            else:
                break

        # print(list_length)
        # print(limit_length_prefix)
        print(prefix)




a = ['flower', 'flow', 'flight']
b = ['xlower', 'flow']
c = []
d = ['', '']
e = ['aaaaaaa', 'aaaaaa', 'aaaaaaa', 'aaaaaaa', 'aaaaaaaa', 'aaaaa']
f = [123, 124, 125, 126, 127]
g = ['123', '124', '125']  # consider 12 as the prefix
h = ['1122', '1133', '2244', '2255', '2266']

common_prefix(h)
