def longestCommonPrefix(strs):
    if not strs:
        return ""

    for i, letter_group in enumerate(zip(*strs)):
        if len(set(letter_group)) > 1:
            return strs[0][:i]
    else:
        return min(strs)

a = ['flower', 'flow', 'flight']
b = ['xlower', 'flow']
c = []
d = ['', '']
e = ['aaaaaaa', 'aaaaaa', 'aaaaaaa', 'aaaaaaa', 'aaaaaaaa', 'aaaaa']
f = [123, 124, 125, 126, 127]
g = ['123', '124', '125']  # consider 12 as the prefix
h = ['1122', '1133', '2244', '2255', '2266']

longestCommonPrefix(h)