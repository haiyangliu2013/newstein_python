# question 2
# use set, we can get set difference very easily
# just use set1 - set2
# this problem, use list to implement your set difference called 'mySetDiff(l1, l2)'
# you can assume list doesn't have duplicate elements


def mySetDiff(l1,l2):
    inters = [objects for objects in l1 if objects in l2]
    diff = [objects for objects in l1 if objects not in inters]

    #>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    # if l1 is not [] and l2 is not []:
    #return diff

    # elif l2 is []:
    #     return l1
    #
    # elif l1 is []:
    #     pass
    #<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    return diff


# l1 = [1, 11.5, 'apple']
# l2 = [3, [1,11.5], 11.5, 'orange']
# print(mySetDiff(l1, l2))

l1 = [1, 11.5, 'apple']
l2 = [3, [1,11.5], 11.5, 'orange']
print(mySetDiff(l2,l1))


l1 = [3, 4, 2, 1]
l2 = []
print(mySetDiff(l1, l2))


l1 = []
l2 = [1, 'apple', 'dog', 4]
print(mySetDiff(l1,l2))

l1 = [1,2,3]
l2 = [1,3]
print(mySetDiff(l1,l2))
