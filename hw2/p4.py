def sum_3(input):
    result = []
    input.sort() # sort
    length = len(input)
    for i in range (length-2):
        # ignore duplicated element
        if i>0 and input[i] == input[i-1]:
            continue

        # i j k

        j = i+1
        k = length-1
        target = -input[i]
        while(j<k):
            tmp = []
            if(input[j]+input[k] == target):
                tmp.extend((input[i], input[j], input[k])) # allow multi elements be appended
                result.append(tmp[:])
                j+=1
                k-=1
                while(j<k and input[j] == input[j-1]):
                    j+=1
                while(j<k and input[k] == input[k+1]):
                    k-=1
            elif (input[j]+input[k]>target):
                k-=1
            else:
                j+=1
    return result

if __name__=='__main__':
    input = [-1,0,1,2,-1,4]
    print(sum_3(input))




