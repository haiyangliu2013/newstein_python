# Question 1
# write a python program to
# a. Convert the list of lists to list of tuple
# b. sort a tuple by its float element descending
# c. convert the result tuple to dictionary


def conversions(list1):
    m = len(list1)
    # condition check
    if type(list1) != list:
        print('Make sure you have a outer list')
        return

    # convert list of list to list of tuple
    for i in range(m):
        if type(list1[i]) == list:
            # >>>>>>>>>>>>>>>>>>>>>>>
            # list1.insert(i+1, tuple(list1[i]))
            # list1.remove(list1[i])

            #<<<<<<<<<<<<<<<<<<<<<<<<<
            list1[i] = tuple(list1[i])
        else:
            print('Make sure you have inner lists')
    # print(list1)

    # sort a tuple by its float element descending
    try:
        #>>>>>>>>>>>>>>>>>>>>>>>
        # list1.sort(key=lambda i:float(i[1]))

        #<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        list1.sort(key=lambda i:i[1], reverse=True)

        #>>>>>>>>>>>>>>>>>>>>>>>
        # list1.reverse()
        print(list1)
    except ValueError:
        print('Check your input')
        return
    except IndexError:
        print('Check your input')
        return

    # convert the result tuple to dictionary
    dict1 = {}
    for i in range(m):
        if type(list1[i]) == tuple:
            dict1[list1[i][0]] = list1[i][1]
    print(dict1)


x = [['item 1', '19', 'a'], ['item2', '15.10'], ['item3', '24.5']]
y = [['apple', '0.8'], ['banana', '0.5'], ['orange', '0.3'], \
    ['watermelon', '2.6'], ['pineapple', '1.9'], ['grape', '1.2']]
z = [['a', '12'], ['b', '25'], ['c', '7'], ['d', '46'], ['f', 'Error']]
i = []
j = [[], [], []]
k = ()
conversions(x)

