# question 2
# we are given an integer N. We need to write a program to find the least positive integer X
# only made up of digits 9's and 0's, such that, X is a multiple of N
# Note: It is assumed that the value of X will not exceed 10^6


# very slow


import re


def specialleastmultiple(N):
    X_list = [9]

    for i in range(90, 100000):    # generate a list of all the possible combinations
        num_digits = len(str(i))
        pattern = "[09]" * num_digits
        if re.match(pattern, str(i)):
            X_list.append(i)

    # print(X_list)

    length = len(X_list)
    for j in range(length):
        if X_list[j] % N == 0:
            return X_list[j]


print(specialleastmultiple(3))
print(specialleastmultiple(5))

#####################

