# We are given an integer N. We need to write a program to find the least positive integer X made up of only digits 9’s and 0’s,
# such that, X is a multiple of N.
# Note: It is assumed that the value of X will not exceed 10^6.
# Examples:
# Input : N = 5
# Output : X = 90
# Exaplanation: 90 is the smallest number made up
# of 9's and 0's which is divisible by 5.
#
# Input : N = 7
# Output : X = 9009
# Exaplanation: 9009 is smallest number made up
# of 9's and 0's which is divisible by 7.

global MAX
global BUFFER

def smallest_multiple():
    global MAX, BUFFER
    MAX = 100000
    BUFFER = []
    queue = []
    queue.append('9')
    for i in range(MAX, 0, -1):
        n1 = queue.pop(0)
        BUFFER.append(n1)
        n2 = n1
        n1+='0'
        queue.append(n1)
        n2+='9'
        queue.append(n2)

def find(n):
    global BUFFER
    for e in BUFFER:
        if(int(e) % n ==0):
            return e


if __name__ == "__main__":
    n=5
    smallest_multiple()
    print(find(n))
