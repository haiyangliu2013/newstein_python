#  Design a stack class  that supports 4 functions: push, pop, top,
# and retrieving the minimum element in constant time( all functions should in O(1) time complexity ).
# def push(x) -- Push element x onto stack.
# def pop() -- Removes the element on top of the stack.
# def top() -- Get the top element.
# def getMin() -- Retrieve the minimum element in the stack.

class MinStack:
    def __init__(self):
        self.q = []

    # @param x, an integer
    # @return an integer
    def push(self, x):
        curMin = self.getMin()
        if curMin == None or x < curMin:
            curMin = x
        self.q.append((x, curMin));

    # @return nothing
    def pop(self):
        self.q.pop()


    # @return an integer
    def top(self):
        if len(self.q) == 0:
            return None
        else:
            return self.q[len(self.q) - 1][0]


    # @return an integer
    def getMin(self):
        if len(self.q) == 0:
            return None
        else:
            return self.q[len(self.q) - 1][1]