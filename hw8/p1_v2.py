#  Given a string of characters. The task is to write a program to print the characters of this string in sorted order using the stack concept.
# You can implement the stack use list or other data containers, but should follow the stack rule, first in last out(FILO) You may look at
# the ASCII table to sort numbers and letters
# Examples:
# 	Input: str = "hello395world216"
# 	Output: 123569dehllloorw

def printSorted(s, l):
    # primary stack
    stack = []

    # secondary stack
    tempstack = []

    # append first character
    stack.append(s[0])

    # iterate for all character in string
    for i in range(1, l):

        # i-th character ASCII
        a = ord(s[i])

        # stack's top element ASCII
        b = ord(stack[-1])

        # if greater or equal to top element
        # then push to stack
        if ((a - b) >= 1 or (a == b)):
            stack.append(s[i])

            # if smaller, then push all element
        # to the temporary stack
        elif ((b - a) >= 1):

            # push all greater elements
            while ((b - a) >= 1):

                # push operation
                tempstack.append(stack.pop())

                # push till the stack is not-empty
                if (len(stack) > 0):
                    b = ord(stack[-1])
                else:
                    break

            # push the i-th character
            stack.append(s[i])

            # push the tempstack back to stack
            while (len(tempstack) > 0):
                stack.append(tempstack.pop())

                # print the stack in reverse order
    print(''.join(stack))


# Driver Code
if __name__=="__main__":
    s = "hello395world216"
    l = len(s)
    printSorted(s, l)
