# Given a binary tree, we need to write a program to print all leaf nodes of the given binary tree from left to right.
# That is, the nodes should be printed in the order they appear from left to right in the given tree. Use Recursion
class treeNode:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

# DFS : detpth first search    BFS: Breath First search
def print_leaf_node(root):
    if root == None:
        return None
    if root.left == None and root.right == None:  # leaf node
        print(root.data)
        return
    if root.left:
        print_leaf_node(root.left)
    if root.right:
        print_leaf_node(root.right)