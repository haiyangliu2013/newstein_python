# You are given the pointer to the head node of a linked list and you need to print
# all its elements in reverse order from tail to head. Use Recursion


import math
import os
import random
import re
import sys


class SinglyLinkedListNode:
    def __init__(self, node_data):
        self.data = node_data
        self.next = None


class SinglyLinkedList:
    def __init__(self):
        self.head = None
        self.tail = None

    def insert_node(self, node_data):
        node = SinglyLinkedListNode(node_data)

        if not self.head:
            self.head = node
        else:
            self.tail.next = node
        self.tail = node


def print_singly_linked_list(node, sep):
    while node:
        print(node.data, end='')
        node = node.next
        if node:
            print(sep, end='')


node1 = SinglyLinkedListNode(16)
node2 = SinglyLinkedListNode(12)
node3 = SinglyLinkedListNode(4)
node4 = SinglyLinkedListNode(2)
node5 = SinglyLinkedListNode(5)

node1.next = node2
node2.next = node3
node3.next = node4
node4.next = node5
node5.next = None

# print(getattr(node1, 'next'))
# print(node1.next)
# print(node1.data)
# print_singly_linked_list(node1, '->')


# write your code here...

def reversePrint_mech(head):
    """
    :head: node
    :return: data corresponding to each node
    """
    if head.next is not None:
        tmp_list.append(head.data)
        reversePrint_mech(head.next)

    else:
        tmp_list.append(head.data)
        tmp_list.reverse()
        for i in range(len(tmp_list)):
            print(tmp_list[i])


def reversePrint(head):
    """
    :head: node
    :print: elements in reverse order from tail to head
    """
    global tmp_list
    tmp_list = []
    reversePrint_mech(head)


reversePrint(node1)
