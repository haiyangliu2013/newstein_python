# question 4
# suppose a list sorted in ascending order is rotated at some pivot unknown to you beforehand
# you are given a target value to search
# if found in the list return its index, otherwise return -1
# 1. you may assume no duplicate exits in the array
# 2. your algorithm's runtime complexity must be in the order of 0(log n)


# not working
def search(lis, target):
    length = len(lis)
    if length % 2 != 0:
        medium = int((length-1)/2)
    if length % 2 == 0:
        medium = int((length-2)/2)

    if target == lis[medium]:
        return medium
    if target == lis[0]:
        return 0
    if target == lis[length-1]:
        return length-1

    if length % 2 == 0:
        lis = lis[1:length+1]
    length = len(lis)

    if target < lis[medium]:
        if target > lis[length-1]:    # choose left
            new_list = lis[0:medium+1]
            search(new_list, target)
        else:
            new_list = lis[medium:length+1]
            search(new_list, target)

    if target > lis[medium]:
        if target < lis[0]:    # choose right
            new_list = lis[medium:length+1]
            search(new_list, target)
        else:
            new_list = lis[0:medium+1]
            search(new_list, target)
    else:
        return -1




a = [4, 5, 6, 7, 0, 1, 2]
print(search(a, 6))
print(search(a, 7))
print(search(a, 3))
print(search(a, 2))
print(search(a, 1))
