#  Suppose a list sorted in ascending order is rotated at some pivot unknown to you beforehand. (i.e., [0,1,2,4,5,6,7] might become [4,5,6,7,0,1,2]).
# You are given a target value to search. If found in the list return its index, otherwise return -1.
# Note: 1. You may assume no duplicate exists in the array.
#           2. Your algorithm's runtime complexity must be in the order of O(log n).
def search( nums, target):
    if not nums:
        return -1

    low, high = 0, len(nums) - 1

    while low <= high:
        mid = (low + high) // 2
        if target == nums[mid]:
            return mid

        if nums[low] <= nums[mid]:
            if nums[low] <= target <= nums[mid]:
                high = mid - 1
            else:
                low = mid + 1
        else:
            if nums[mid] <= target <= nums[high]:
                low = mid + 1
            else:
                high = mid - 1

    return -1


    #return l if target in nums[l:l+1] else -1


if __name__ == "__main__":
    arr = [4,5,6,7,0,1,2]
    print(search(arr, 5))