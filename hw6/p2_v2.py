# 2.1 Use class to make a bank account class with support for deposit and
# withdraw operations.
# 	class BankAccount:
# 		...
# 2.2 Create another class called minimumBankAccount which is
# inheritanted from BankAccount, and if balance is lower than 1500, you
# can’t withdraw the balance and should informed the user by printing some
# Text

class BankAccount:
    def __init__(self, balance):
        self.balance = balance


    def deposit(self, amount):
        self.balance += amount
        return self.balance


    def withdraw(self, amount):
        if amount > self.balance:
            return ("INVALID TRANSACTION")
        self.balance -= amount
        return self.balance


class MinimumBankAccount(BankAccount):
    def withdraw(self, amount):
        if self.balance - amount <1500:
            return ("Fail! Your Balance will lower than 1500");