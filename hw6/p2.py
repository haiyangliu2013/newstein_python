# question 2
# 2.1 Use class to make a bank account with support for deposit and withdraw operations
# 2.2 Create another class called minimumBankAccount which is inherited
# from BankAccount, and if balance is lower than 1500, you can't withdraw
# the balance and should informed the user by printing some Text.


class BankAccount:
    """A Bank Account with initial balance = 0"""
    balance = 0

    def __init__(self, username):
        """Initializes the data"""
        self.username = username

    def checkBalance(self):
        print('Your current balance is: ', BankAccount.balance)

    def deposit(self, amount):
        print('Your previous balance is: ', BankAccount.balance)
        BankAccount.balance += amount
        print('Your current balance is: ', BankAccount.balance)

    def withdraw(self, amount):
        print('Your previous balance is: ', BankAccount.balance)
        BankAccount.balance -= amount
        print('Your current balance is: ', BankAccount.balance)



class MinimumBankAccount(BankAccount):
    min_balance = 1500

    def __init__(self, username):
        BankAccount.__init__(self, username)

    def withdraw(self, amount):
        if MinimumBankAccount.balance - amount >= MinimumBankAccount.min_balance:
            super().withdraw(amount)
        else:
            print('Low balance. Unable to withdraw')



myaccount = MinimumBankAccount('Jason')


myaccount.deposit(25000)
myaccount.withdraw(1)
myaccount.checkBalance()
myaccount.withdraw(100000)
myaccount.withdraw(23499)

