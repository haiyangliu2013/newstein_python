# question 3
# given a binary tree, we need to write a program to print all leaf nodes of the given tree from left to right
# that is, the nodes should be printed in the order they appear from left to right


class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


node1 = Node(1)
node2 = Node(2)
node3 = Node(3)
node4 = Node(4)
node5 = Node(5)
node6 = Node(6)
node7 = Node(7)
node1.left = node2
node1.right = node3
node2.left = node4
node2.right = node5
node3.left = node6
node3.right = node7


def binarymech(head):
    """Returns a list with elements, from left to right, in the binary tree"""
    index = tmp_list.index(head.data)
    if head.left and head.right is not None:    # e.g. [1] to [2, 1, 3]
        tmp_list.insert(index, head.left.data)
        tmp_list.insert(index+2, head.right.data)
        binarymech(head.left)
        binarymech(head.right)

    if head.left is not None and head.right is None:    # e.g. [1] to [2, 1]
        tmp_list.insert(index, head.left.data)
        binarymech(head.left)

    if head.right is not None and head.left is None:    # e.g. [1] to [1, 3]
        tmp_list.insert(index+1, head.right.data)
        binarymech(head.right)


def binaryPrint(head):
    """Prints the elements"""
    global tmp_list
    tmp_list = []
    tmp_list.append(head.data)    # appends the first element into the list

    binarymech(head)

    for i in range(len(tmp_list)):
        print(tmp_list[i])


binaryPrint(node1)


