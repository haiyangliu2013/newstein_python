import math
import os
import random
import re
import sys

class SinglyLinkedListNode:
    def __init__(self, node_data):
        self.data = node_data
        self.next = None

class SinglyLinkedList:
    def __init__(self):
        self.head = None
        self.tail = None

    def insert_node(self, node_data):
        node = SinglyLinkedListNode(node_data)

        if not self.head:
            self.head = node
        else:
            self.tail.next = node


        self.tail = node

def print_singly_linked_list(node, sep, fptr):
    while node:
        fptr.write(str(node.data))

        node = node.next

        if node:
            fptr.write(sep)

def reverse(head):
    if head == None: # no node
        return None
    if head.next==None: # only 1 node
        print(head.data)
        return
    else:
        reverse(head.next)
    print(head.data)
    return

if __name__ == "__main__":
    ll = SinglyLinkedList()
    ll.insert_node(1)
    ll.insert_node(2)
    ll.insert_node(3)
    ll.insert_node(4)
    ll.insert_node(5)
    reverse(ll.head)
    
