# Given a string of characters. The task is to write a program to print the
# characters of this string in sorted order using the stack concept
# you can implement the stack use list or other data containers
# but should follow the stack rule, first in last out. Use 2 Stacks to sort


# The program doesn't work
def sortwithstack(string):
    stack1 = [i for i in string]
    stack2 = []
    stack2.append(stack1.pop())

    while stack1 is not []:

        if ord(str(stack1[-1])) > ord(str(stack2[-1])):
            stack2[-1], stack1[-1] = stack1[-1], stack2[-1]

        elif ord(str(stack1[-1])) < ord(str(stack2[-1])):
            stack2.append(stack1.pop())
            continue

        j = True
        while len(stack2) >= 2 and j:
            if ord(str(stack2[-1])) > ord(str(stack2[-2])):
                stack1.append(stack2.pop())
                stack2[-1], stack1[-1] = stack1[-1], stack2[-1]
                if ord(str(stack2[-1])) <= ord(str(stack2[-2])):
                    j = False

        print(stack2)
        print(stack1)


string = 'Helloworld'
sortwithstack(string)


