# dfs recursively
import collections

class node:
    def __init__(self, val):
        self.val = val
        self.left = None
        self.right = None


class traverse_tree:
    def levelOrderBottom1(self, root):
        res = []
        self.dfs(root, 0, res)
        return res


    def dfs(self, root, level, res):
        if root:
            if len(res) < level + 1:
                res.insert(0, [])
            res[-(level + 1)].append(root.val)
            self.dfs(root.left, level + 1, res)
            self.dfs(root.right, level + 1, res)


    # dfs + stack
    def levelOrderBottom2(self, root):
        stack = [(root, 0)]
        res = []
        while stack:
            node, level = stack.pop()
            if node:
                if len(res) < level + 1:
                    res.insert(0, [])
                res[-(level + 1)].append(node.val)
                stack.append((node.right, level + 1))
                stack.append((node.left, level + 1))
        return res


    # bfs + queue
    def levelOrderBottom(self, root):
        # make a tuple (node, level)
        queue, res = collections.deque([(root, 0)]), []
        while queue:
            # pop the first element
            node, level = queue.popleft()
            if node:
                # has next level, insert a container before
                if len(res) < level + 1:
                    res.insert(0, [])
                # append from end to front
                res[-(level + 1)].append(node.val)
                # enqueue
                queue.append((node.left, level + 1))
                queue.append((node.right, level + 1))
        return res

if __name__ == "__main__":
    n1 = node(3)
    n2 = node(9)
    n3 = node(20)
    n4 = node(15)
    n5 = node(7)

    n1.left = n2
    n1.right = n3
    n2.left = None
    n2.right = None
    n3.left = n4
    n3.right = n5
    n4.left = None
    n4.right = None
    n5.left = None
    n5.right = None

s = traverse_tree()
print(s.levelOrderBottom(n1))



