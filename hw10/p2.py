# question 2
# Write a python 3 program to parse and get the titles, source, and the date of all articles from
# https://finance.google.com/finance/market_news. The program returns the a list of lists,
# where each list contains the title, source and the date of an article. Write all these to a file
# named top10articles.txt, one line for each article, and with comma separated values.


# HTTP Error 403: Forbidden
from bs4 import BeautifulSoup
from urllib import request


def take_and_record():
    list1 = []

    url = 'https://finance.google.com/finance/market_news'
    page = request.urlopen(url).read()
    soup = BeautifulSoup(page, 'html.parser')    # BeautifulSoup
    print(soup)

    # title -- class = 'Igo7ld'
    # source -- class = 'wqg8ad' span
    # date -- class = 'FGlSad' span

    title = soup.find_all('span', class_='Igo7ld')
    l_title = []
    print(title)
    for i in title:
        a = (i.text).rstrip().encode('ascii', 'ignore')
        a.replace('\n', '')
        l_title.append(a)
    list1.append(l_title)
    print(list1)

    source = soup.find_all('span', class_='wqg8ad')
    l_source = []
    for i in source:
        a = (i.text).rstrip().encode('ascii', 'ignore')
        a.replace('\n', '')
        l_source.append(a)
    list1.append(l_source)

    date = soup.find_all('span', 'FGlSad')
    l_date = []
    for i in date:
        a = (i.text).rstrip().encode('ascii', 'ignore')
        a.replace('\n', '')
        l_date.append(a)
    list1.append(l_date)

    return 1


f = open('top10articles.txt', 'w')
l = take_and_record()

for i in range(10):
    s1 = l[0][i]
    s2 = l[1][i]
    s3 = l[2][i]
    s = s1 + ',' + s2 + ',' + s3
    f.write(s)
f.close()




