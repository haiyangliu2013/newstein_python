# question 5
# Given a binary tree, return the bottom-up level order traversal of its nodes' values
# ie, from left to right, level by level from leaf to root


class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


def printtree(head, n):
    """
    Input: head = head of the tree.  n = number of layers
    Output: a list of lists, containing elements from leaf to root
    """

    traversal = [[] for x in range(n)]
    var = head
    layer = 1

    if head:
        traversal[0].append(head.data)

    if head is None:
        return None

    upper_var = [head]                                 # holds the elements that have not been stored into the traversal list
    while len(upper_var) != 0:
        if var.left:                                   # if left branch exists
            traversal[layer].append(var.left.data)     # add its data
            upper_var.append(var)
            var = var.left                             # go to left branch
            layer += 1

        elif var.right:                                # if right branch exists
            traversal[layer].append(var.right.data)    # add its data
            upper_var.append(var)
            var = var.right                            # go to right branch
            layer += 1

        else:
            if upper_var[-1].left == var:
                upper_var[-1].left = None
            if upper_var[-1].right == var:
                upper_var[-1].right = None

            var = upper_var.pop()
            layer -= 1
        # print(traversal)

    traversal.reverse()
    return traversal


if __name__ == '__main__':
    node1 = Node(1)
    node2 = Node(2)
    node3 = Node(3)
    node4 = Node(4)
    node5 = Node(5)
    node6 = Node(6)
    node7 = Node(7)
    node1.left = node2
    node1.right = node3
    node2.left = node4
    node2.right = node5
    node3.left = node6
    node3.right = node7
    print(printtree(node1, 3))

    node1 = Node(1)
    node2 = Node(2)
    node3 = Node(3)
    node4 = Node(4)
    node5 = Node(5)
    node6 = Node(6)
    node7 = Node(7)
    node1.left = node2
    node1.right = node3
    node2.right = node5
    node3.right = node7
    print(printtree(node1, 3))

    node1 = Node(1)
    node2 = Node(2)
    node3 = Node(3)
    node4 = Node(4)
    node5 = Node(5)
    node6 = Node(6)
    node7 = Node(7)
    node1.left = node2
    node1.right = node3
    node3.left = node6
    node3.right = node7
    print(printtree(node1, 3))







