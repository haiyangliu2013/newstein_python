# Shortest version
# if first sublist is non-duplicated, it will stop
# def remove_duplicate(list):
#    f = True
#    n = 0
#
#    while f:
#        single_list = list[n]
#        # print(single_list)  # check
#
#        noccurence = int(list.count(single_list))
#
#        if noccurence >= 2:
#            list.remove(single_list)
#
#        if noccurence == 1:
#            # print('No change')    # check
#            f = False
#    print(list)
#
# # L = [[10, 20], [40], [30, 56, 25], [10, 20], [33], [40]]
# L = [[1],[10, 20], [40], [30, 56, 25], [10, 20], [33], [40]] ##??
# remove_duplicate(L)


# def remove_duplicate(list):
#    f = True
#    n = 0
#    nl = int(len(list))
#
#    while f:
#        single_list = list[n]
#        print(single_list)  # check
#
#        noccurence = int(list.count(single_list))
#
#        if noccurence >= 2:
#            list.remove(single_list)
#            n += 1
#
#        elif noccurence == 1:
#            print('No change')
#            n += 1
#            f = False
#
#        elif n == nl:
#            print('You have reached the end of the list.')
#        print(list)
#
#
# L = [[10, 20], [40], [30, 56, 25], [10, 20], [33], [40]]
# remove_duplicate(L)

# def remove_duplicate(alistoflists):
#    n = int(len(alistoflists))
#    print(n)    # check
#
#    for i in range(0, n+1):
#        print(i)
#        single_list = alistoflists[i]
#
#        n = int(alistoflists.count(single_list))
#
#        if n >= 2:
#            alistoflists.remove(single_list)
#
#        elif n == 1:
#            print('No change')
#
#
# L = [[10, 20], [40], [30, 56, 25], [10, 20], [33], [40]]
#
# remove_duplicate(L)



# for i in range(10,5,-1): # [x,y) include x, exclude y
#     print(i)

# l=[1,2,3,4,5]
# a=[]
# for n in l:
#     n+=1
#     a.append(n)
#
# print(a)

# O(1) < O(log(n)) < O(n) < O(nlog(n)) < O(n^2)



# def remove_duplicate(l):
#     i=0
#     while i < len(l):
#         print(len(l))
#         n=l.count(l[i])
#         if n>1:
#             l.remove(l[i])
#
#         else:
#             i+=1
#             continue
#
# L = [[10, 20], [40], [30, 56, 25], [10, 20], [33], [40]]
# remove_duplicate(L)
# print(L)