def part_two():
   first_name = input('\n\nPlease enter your first name: ')
   last_name = input('Please enter your last name: ')

   print('Welcome to Python, {0} {1}'.format(first_name, last_name))


def part_three():
   print('\n\nLet me show you something fun')
   x = '22'
   y = '33'
   print('The sum of x and y (in integer) is', int(x) + int(y))

   x = '31'
   y = '44'
   print('The sum of x and y (in string) is', int(x + y))


f = True


while f:
   choice = int(input('''You have two choices:
       If you enter 1, you will enter the welcoming screen;
       If you enter 2, you will see some operations;
       Otherwise, you will be redirected to this page.
       Please enter a number: '''))

   if choice == 1:
       part_two()
       f = False

   elif choice == 2:
       part_three()
       f = False

   else:
       print('returned')
       print('')
       print('')
       continue


print('Done')

# if user enter non-numerical input, will cause error