#this can only work when mylist_2 has 3 elements
# def is_Sublist(mylist_1, mylist_2):
#    x = mylist_2[0]
#    y = mylist_2[1]
#    z = mylist_2[2]
#
#    try:
#        i = mylist_1.index(x)       # consecutive
#        if x == mylist_1[i] and\
#            y == mylist_1[i+1] and\
#            z == mylist_1[i+2]:
#            return True
#
#        else:
#            return False
#
#    except ValueError:
#        return False
#
#
# a = [1, 3, 5, 6, 7, 9]
# b = [3, 5, 6]
# print(is_Sublist(a, b))


#method 2  also return true if non-consecutive list appear
# def is_Sublist(mylist_1, mylist_2):
#    x = [numbers for numbers in mylist_1 if numbers in mylist_2]
#    print(x)
#
#    if x == mylist_2:
#        return True
#    elif x == mylist_1:
#        return True
#    else:
#        return False
#
#
# a = [1, 4, 7, 8, 9]
# b = [7, 9]
# print(is_Sublist(a, b))


def is_Sublist(mylist_1, mylist_2):
   str1 = ''.join(str(i) for i in mylist_1)    # '' is the separator of concatenation, join from list(not number) to str, convert first
   str2 = ''.join(str(i) for i in mylist_2)

   print(str1)
   print(str2)
   # print(str2 in str1)
   if not mylist_1 and not mylist_2:  # not(l1==[] or l2==[]) -> not l1 ==[] and not l2==[]
        return False

   try:
       #assert mylist_2 in mylist_1
       assert str2 in str1
       return True
   except AssertionError:
       return False



a = [1, 4, 5, 6, 9]
b = [5,6,9]
print(is_Sublist(a, b))
