
def max_sum_subseq(list, i, n, prev):
    # base case: all elements are processed
    if i==n:
        return 0

    # recurse by excluding current element
    excl = max_sum_subseq(list, i+1, n, prev)
    incl = 0

    # include current element only if it is not adjacent to previous element considered

    if (prev+1) != i:
        incl = max_sum_subseq(list, i+1, n, i)+ list[i]

    # return maximum sum we get by including or excluding
    # current item
    return max(incl, excl)

if __name__ == '__main__':
    l = [1, 2, 9, 4, 5, 0, 4, 11, 6]
    sum = max_sum_subseq(l, 0, len(l), 0)
    print(sum)