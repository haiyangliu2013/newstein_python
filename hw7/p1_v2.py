# / (January|February|March|April|May|June|July|August|September|October|November|December)[ ](0[1-9]|[12][0-9]|30)[,][ ](\d{4}) /
#
#
#
#
#
# -----#1 match
# -3.3F
# +44.44F
# -7.22F
# -----#1 not match
# +3.44f
# -7..27f
# +3.arF
#
# -----#2 match
# #1
# one
# oneone
# -----#2 not match
# two
# #2
# 3
#
# -----#3 match
# a3 a
# 44 4
# c7 c
# -----#3 not match
# abc
# 123
# a234a
#
#
