import pymysql as ps

db = ps.connect("localhost", "root", "0811", "db2")


# prepare a cursor object using cursor() method
cursor = db.cursor()

# Prepare SQL query to UPDATE required records
sql =  " update person set firstname ='LaunchPad' where lastname = 'liu' "
try:
   # Execute the SQL command
   cursor.execute(sql)
   # Commit your changes in the database
   db.commit()
except:
   # Rollback in case there is any error
   db.rollback()


# fetch all of the rows from the query

data = cursor.fetchall ()
#print(data)

# print the rows
for row in data :
   print (row[0], row[1])

# close the cursor object
cursor.close ()



# disconnect from server
db.close()

#insert into person values ('haha', 'dodo');