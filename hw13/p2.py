# question 2
# Given a linked list, remove the n-th node from the end of list and return its head
# Do this in one pass


class ListNode():
    def __init__(self, data):
        self.data = data
        self.next = None

    def printlist(self, head):
        print(head.data)
        if head.next:
            self.printlist(head.next)


# Use two pointers, one reference & one main. First set reference pointer to n * next
# Move both pointers forward, if ref.next=End, remove main
def remove_nth_node(head, n):
    """
    Input:  head -- the head of a singly linked list
            n    -- nth node you want to remove
    Output: returns the head of the altered list
    """
    p_ref = head
    for i in range(n):    # let reference pointer move n steps first
        p_ref = p_ref.next

    if p_ref is None:    # only happens if n = length of the singly linked list
        tmp = head.next
        del head
        return tmp

    p_main = head
    while p_ref.next:    # move both pointers forward
        p_main = p_main.next
        p_ref = p_ref.next

    # now p_main.next is the node we want to remove
    tmp = p_main.next.next
    del p_main.next
    p_main.next = tmp

    return head


if __name__ == "__main__":
    n1 = ListNode(1)
    n2 = ListNode(2)
    n3 = ListNode(3)
    n4 = ListNode(4)
    n5 = ListNode(5)
    n6 = ListNode(6)
    n7 = ListNode(7)
    n1.next = n2
    n2.next = n3
    n3.next = n4
    n4.next = n5
    n5.next = n6
    n6.next = n7
    head1 = remove_nth_node(n1, 3)
    head1.printlist(head1)
    print("\n")

    head2 = remove_nth_node(n1, 6)
    head2.printlist(head2)
