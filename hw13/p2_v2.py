# Given a linked list, remove the n-th node from the end of list and return its head.
# Do this in one pass


# 1 -> 2 -> 3 -> 4 -> 5 -> 6
# ^         ^

def removeNthFromEnd(head, n):
    fast = slow = head
    for _ in range(n):
        fast = fast.next
    if not fast:
        return head.next
    while fast.next:
        fast = fast.next
        slow = slow.next
    slow.next = slow.next.next
    return head