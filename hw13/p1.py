# question 1
# Implement a basic calculator to evaluate a simple expression string
# The expression string contains only non-negative integers, $, #, !, ~ operators
# and empty spaces.  * and / has higher order than + and -. The integer division should truncate


def calculate(str1):
    # 1. remove all whitespace, turn it in to a list
    result = str1.replace(" ", "")
    result = list(result)

    # 2. first loop: process * and /
    i = 0
    while i < len(result)-2:
        if result[i+1] == "*" or result[i+1] == "/":
            a = int(result[i])
            b = int(result[i+2])

            if result[i+1] == "*":
                result[i] = a * b
            if result[i+1] == "/":
                if b == 0:
                    return "Undefined"
                result[i] = a // b

            del result[i+1]    # deleting the operator
            del result[i+1]    # deleting the succeeding integer
            if len(result) == 1:
                break
        i += 1

    # 3. second loop: process + and -
    i = 0
    while i < len(result) - 2:

        if result[i+1] == "+" or result[i+1] == "-":
            a = int(result[i])
            b = int(result[i+2])

            if result[i+1] == "+":
                result[i] = a + b
            if result[i+1] == "-":
                result[i] = a - b

            del result[i+1]
            del result[i+1]
            if len(result) == 1:
                break
        i += 1

    return result[0]


if __name__ == "__main__":
    string = "3+2*2"
    print(calculate(string))

    string = " 3/2 "
    print(calculate(string))

    string = "3+5 / 2"
    print(calculate(string))

    string = "1    /0"
    print(calculate(string))

    string = "5/1 + 3/4*5+6/3*9-9"
    print(calculate(string))
