# Implement a basic calculator to evaluate a simple expression string.
# The expression string contains only non-negative integers, +, -, *, /
# operators and empty spaces . * and / has higher order than + and -  The integer division should truncate toward zero.


# use stack to do this problem
def calculate(s):
    if not s:
        return "0"
    stack, num, sign = [], 0, "+"
    for i in range(len(s)):
        # char is digit, calculate the number
        if s[i].isdigit():
            num = num*10+ord(s[i])-ord("0")
        # not digit and not space or last element, should finish up
        # if + or - , push to stack
        # if * or / , pop , calculate then push again
        if (not s[i].isdigit() and not s[i].isspace()) or i == len(s)-1:
            if sign == "-":
                stack.append(-num)
            elif sign == "+":
                stack.append(num)
            elif sign == "*":
                stack.append(stack.pop()*num)
            else:
                tmp = stack.pop()
                if tmp//num < 0 and tmp%num != 0:
                    stack.append(tmp//num+1)
                else:
                    stack.append(tmp//num)
            sign = s[i]
            num = 0
    return sum(stack)

str = "2+8*2"
print(calculate(str))

# stack : 2 ,   sign : +

# stack : 8 ,   sign : *

# stack : 2 , 16   sign : _